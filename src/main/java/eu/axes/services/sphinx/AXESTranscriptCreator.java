package eu.axes.services.sphinx;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.SerializationUtils;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.extended.factory.SegmentFactory;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.Audio;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.MediaUnit;
import org.ow2.weblab.core.model.TemporalSegment;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.core.services.ContentNotAvailableException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.service.transcript.sphinx.ATranscriptCreator;
import org.ow2.weblab.service.transcript.sphinx.SphinxTranscriber;

import edu.cmu.sphinx.linguist.dictionary.Word;
import edu.cmu.sphinx.result.WordResult;
import eu.axes.utils.AXES;
import eu.axes.utils.AxesAnnotator;
import eu.axes.utils.NamingSchemeUtils;


/**
 * Specialisation of the Transcript Creator abstraction, that enables to select the files that has to be transcribed from the document and that annotate the document for each file and associated media
 * unit.
 *
 * @author ymombrun
 * @date 2015-01-26
 */
public class AXESTranscriptCreator extends ATranscriptCreator {


	private static final String WAV = "wav";


	private final URI producer;


	private final File cacheFolder;


	/**
	 * The base constructor of the class.
	 *
	 * @param sphinxTranscriber
	 *            The class in charge of really calling Sphinx and handling its results.
	 */
	public AXESTranscriptCreator(final SphinxTranscriber sphinxTranscriber) {
		this(sphinxTranscriber, null);
	}


	/**
	 * The base constructor of the class.
	 *
	 * @param sphinxTranscriber
	 *            The class in charge of really calling Sphinx and handling its results.
	 * @param cacheFolder
	 *            The path to the folder that should store the results for caching purposes.
	 */
	public AXESTranscriptCreator(final SphinxTranscriber sphinxTranscriber, final String cacheFolder) {
		this(sphinxTranscriber, cacheFolder, null);
	}


	/**
	 * @param sphinxTranscriber
	 *            The class in charge of really calling Sphinx and handling its results.
	 * @param cacheFolder
	 *            The path to the folder that should store the results for caching purposes.
	 * @param producerUri
	 *            The URI to be annotated in produced by statements. Might be null. In that case no statement will be added.
	 */
	public AXESTranscriptCreator(final SphinxTranscriber sphinxTranscriber, final String cacheFolder, final String producerUri) {
		super(sphinxTranscriber);
		if (producerUri == null) {
			this.producer = null;
		} else {
			this.producer = URI.create(producerUri);
		}
		if (cacheFolder != null) {
			final File theCacheFolder = new File(cacheFolder);
			if (!theCacheFolder.isAbsolute()) {
				this.log.warn("The cache folder path " + cacheFolder + " is not absolute. Folder " + theCacheFolder.getAbsolutePath() + " will be used.");
			}
			if (!theCacheFolder.exists() && !theCacheFolder.mkdirs()) {
				this.log.error("Unable to create directory " + theCacheFolder + ". Cache will not be activated.");
				this.cacheFolder = null;
			} else if (!theCacheFolder.isDirectory()) {
				this.log.error("Unable to create directory " + theCacheFolder + " since a file is already there. Cache will not be activated.");
				this.cacheFolder = null;
			} else {
				this.cacheFolder = theCacheFolder;
			}
		} else {
			this.cacheFolder = null;
		}
		this.log.debug("AXESTranscriptCreator created using " + this.cacheFolder + " as cache folder.");
	}


	@Override
	protected Map<MediaUnit, File> retrieveMapOfMediaUnit(final Document document) throws ContentNotAvailableException {
		if (document.getMediaUnit().isEmpty()) {
			final String message = "Not a single MediaUnit in document " + document.getUri() + ".";
			this.log.error(message);
			throw ExceptionFactory.createContentNotAvailableException(message);
		}
		final MediaUnit mu = document.getMediaUnit().get(0);

		final AxesAnnotator aa = new AxesAnnotator(mu);
		final URI wavContentUri = NamingSchemeUtils.getVideoResourceURI(aa.readCollectionId().firstTypedValue(), aa.readVideoId().firstTypedValue(), AXESTranscriptCreator.WAV);
		final File wavFile;
		try {
			wavFile = this.contentManager.readContent(wavContentUri, mu);
		} catch (final WebLabCheckedException wlce) {
			final String message = "Unable to find a wav file as normalised content on " + document.getUri() + ".";
			this.log.error(message, wlce);
			throw ExceptionFactory.createContentNotAvailableException(message, wlce);
		}

		return Collections.singletonMap(document.getMediaUnit().get(0), wavFile);
	}


	@Override
	protected void transcribeFile(final File audioFile, final MediaUnit mainVideoUnit, final Document document) throws UnexpectedException, ContentNotAvailableException {

		final File cacheFile = this.lookupCacheFile(document);

		final LinkedList<ASRSegment> segments; // Need to reference implementation to ensure that it is serialisable
		if (cacheFile == null || !cacheFile.exists()) {
			this.log.info("No preprocessed file " + cacheFile + ". Calling Sphinx.");
			final List<List<WordResult>> tokens = this.transcribeFile(audioFile);
			segments = this.extractSegments(tokens);
			if (cacheFile != null) {
				if (!cacheFile.getParentFile().exists() && !cacheFile.getParentFile().mkdirs()) {
					this.log.warn("Unable to create folder to store cache file " + cacheFile + ".");
				} else {
					try (FileOutputStream fos = new FileOutputStream(cacheFile)) {
						SerializationUtils.serialize(segments, fos);
					} catch (final IOException ioe) {
						this.log.warn("An error occured while serialising the cache file " + cacheFile + ". You should remove it if needed.");
					}
				}
			}
		} else {
			this.log.info("A cache file " + cacheFile + " already exists. Result will be loaded from it.");
			try (final FileInputStream fis = new FileInputStream(cacheFile)) {
				segments = SerializationUtils.deserialize(fis);
			} catch (final IOException ioe) {
				final String message = "An error occured when trying to deserialise cache file " + cacheFile + ".";
				this.log.error(message, ioe);
				throw ExceptionFactory.createUnexpectedException(message, ioe);
			}
		}

		if (segments.isEmpty()) {
			this.log.warn("Not a single segment found in file " + audioFile + ".");
			return;
		}

		final WProcessingAnnotator wpaMain = this.createWPA(mainVideoUnit);
		final URI mainVideoUri = URI.create(mainVideoUnit.getUri());
		for (final ASRSegment asrSegment : segments) {
			final TemporalSegment segment = SegmentFactory.createAndLinkTemporalSegment(mainVideoUnit, asrSegment.getStart(), asrSegment.getEnd());
			final URI segmentUri = URI.create(segment.getUri());
			final Audio audio = WebLabResourceFactory.createAndLinkMediaUnit(document, Audio.class);
			final Text text = WebLabResourceFactory.createAndLinkMediaUnit(document, Text.class);
			text.setContent(asrSegment.getText());

			final WProcessingAnnotator wpaAudio = this.createWPA(audio);
			wpaAudio.writeSoundtrackOf(mainVideoUri);
			wpaAudio.writeLocatedAt(segmentUri);

			final WProcessingAnnotator wpaText = this.createWPA(text);
			wpaText.writeTranscriptOf(mainVideoUri);
			wpaText.writeLocatedAt(segmentUri);

			wpaMain.startInnerAnnotatorOn(segmentUri);
			wpaMain.writeType(AXES.CLASSES.SPEECH_SEGMENT);
			wpaMain.writeDelimiterOfSoundtrackUnit(URI.create(audio.getUri()));
			wpaMain.writeDelimiterOfTranscriptUnit(URI.create(text.getUri()));
			wpaMain.endInnerAnnotator();
		}

	}


	private LinkedList<ASRSegment> extractSegments(final List<List<WordResult>> tokens) {
		final LinkedList<ASRSegment> segments;
		segments = new LinkedList<>();
		for (final List<WordResult> sentence : tokens) {
			final StringBuilder sb = new StringBuilder();
			final Iterator<WordResult> sentenceIterator = sentence.iterator();
			long start = -1, end = -1;
			while (sentenceIterator.hasNext()) {
				final WordResult result = sentenceIterator.next();
				if (start == -1) {
					start = result.getTimeFrame().getStart();
				}
				end = result.getTimeFrame().getEnd();
				final Word word = result.getWord();
				final String spelling = word.getSpelling().replace("_", " ");
				if (spelling.startsWith("-") && sb.length() > 0 && sb.charAt(sb.length() - 1) == '0') {
					sb.deleteCharAt(sb.length() - 1);
				}
				sb.append(spelling);
				if (!spelling.endsWith("'")) {
					sb.append(' ');
				}
				sentenceIterator.remove();
			}

			final String transcribedString = sb.toString().trim();
			if ((!transcribedString.isEmpty()) && (start < end) && (start >= 0) && (end < Integer.MAX_VALUE)) {
				segments.add(new ASRSegment((int) start, (int) end, transcribedString));
			} else {
				this.log.warn("Something wrong with the segment of start " + start + ", end " + end + " and text: " + transcribedString + ".");
			}
		} // End for each token
		return segments;
	}


	private List<List<WordResult>> transcribeFile(final File audioFile) throws UnexpectedException {
		final List<List<WordResult>> tokens;
		try (final FileInputStream fis = new FileInputStream(audioFile)) {
			tokens = this.sphinxTranscriber.transcribe(fis, false);
		} catch (final Exception e) {
			final String message = "An error occured while trying to process file " + audioFile + ".";
			this.log.error(message, e);
			throw ExceptionFactory.createUnexpectedException(message, e);
		}
		return tokens;
	}


	private File lookupCacheFile(final Document document) {
		if (this.cacheFolder == null) {
			return null;
		}
		final AxesAnnotator axesAnnotator = new AxesAnnotator(document);
		final String collectionId = axesAnnotator.readCollectionId().firstTypedValue();
		final String videoId = axesAnnotator.readVideoId().firstTypedValue();
		return new File(new File(new File(this.cacheFolder, collectionId), videoId), videoId + ".asr");
	}


	private WProcessingAnnotator createWPA(final MediaUnit mediaUnit) {
		final Annotation annot = WebLabResourceFactory.createAndLinkAnnotation(mediaUnit);
		final WProcessingAnnotator wpa = new WProcessingAnnotator(URI.create(mediaUnit.getUri()), annot);
		if (this.producer != null) {
			wpa.startInnerAnnotatorOn(URI.create(annot.getUri()));
			wpa.writeProducedBy(this.producer);
			wpa.endInnerAnnotator();
		}
		return wpa;
	}

}
