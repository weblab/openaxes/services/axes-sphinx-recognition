package eu.axes.services.sphinx;

import java.io.Serializable;


/**
 * @author ymombrun
 * @date 2015-01-28
 */
public class ASRSegment implements Serializable {


	private static final long serialVersionUID = 4401688197840372416L;


	private final int start;


	private final int end;


	private final String text;


	/**
	 * @param start
	 *            Start of the segment in ms since the begining of the video
	 * @param end
	 *            End of the segment in ms since the begining of the video
	 * @param text
	 *            The text found at location
	 */
	public ASRSegment(final int start, final int end, final String text) {
		super();
		this.start = start;
		this.end = end;
		this.text = text;
	}


	/**
	 * @return the start
	 */
	public int getStart() {
		return this.start;
	}


	/**
	 * @return the end
	 */
	public int getEnd() {
		return this.end;
	}


	/**
	 * @return the text
	 */
	public String getText() {
		return this.text;
	}


}
