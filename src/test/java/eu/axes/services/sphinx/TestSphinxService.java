/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version
 * 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 */
package eu.axes.services.sphinx;

import java.io.File;

import org.junit.Test;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.service.transcript.sphinx.SphinxTranscriber;
import org.ow2.weblab.service.transcript.sphinx.SphinxTranscriptorService;

import edu.cmu.sphinx.api.Configuration;

/**
 * Testing the service with various inputs and various configurations.
 */
public class TestSphinxService {


	@Test
	public void testSphinxSimpleOutputEn() throws Exception {
		final Configuration configuration = new Configuration();
		configuration.setAcousticModelPath("resource:/edu/cmu/sphinx/models/acoustic/wsj");
		configuration.setDictionaryPath("resource:/edu/cmu/sphinx/models/acoustic/wsj/dict/cmudict.0.6d");
		configuration.setLanguageModelPath("resource:/edu/cmu/sphinx/models/language/en-us.lm.dmp");

		final SphinxTranscriptorService transcriber = new SphinxTranscriptorService(new AXESTranscriptCreator(new SphinxTranscriber(configuration, "src/test/resources/SphinxTestConfig.xml")));

		final WebLabMarshaller marshaller = new WebLabMarshaller();
		final Document doc = marshaller.unmarshal(new File("src/test/resources/vthe_daily_2011_09_16_cnn_short.xml"), Document.class);
		final ProcessArgs pa = new ProcessArgs();
		pa.setResource(doc);

		transcriber.process(pa);

		marshaller.marshalResource(doc, new File("target/outputEn.xml"));
	}



	@Test
	public void testSphinxSimpleOutputFr() throws Exception {
		final Configuration configuration = new Configuration();
		configuration.setAcousticModelPath("resource:/fr/lium/models/acoustic");
		configuration.setDictionaryPath("resource:/fr/lium/models/dict/frenchWords62K.dic");
		configuration.setLanguageModelPath("resource:/fr/lium/models/language/french3g62K.lm.dmp");

		final SphinxTranscriptorService transcriber = new SphinxTranscriptorService(new AXESTranscriptCreator(new SphinxTranscriber(configuration, "src/test/resources/SphinxTestConfig.xml")));

		final WebLabMarshaller marshaller = new WebLabMarshaller();
		final Document doc = marshaller.unmarshal(new File("src/test/resources/vTestFr.xml"), Document.class);
		final ProcessArgs pa = new ProcessArgs();
		pa.setResource(doc);

		transcriber.process(pa);

		marshaller.marshalResource(doc, new File("target/outputFr.xml"));
	}

}
